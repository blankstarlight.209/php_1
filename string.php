<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        
        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5

        $kalimat1 = "Hello PHP!";
        $panjangKalimat1 = strlen($kalimat1);
        $jumlahKataKalimat1 = str_word_count($kalimat1);

        echo "Kalimat 1 : $kalimat1 <br>";
        echo "panjang Kalimat1 : $panjangKalimat1 <br> ";
        echo "jumlah Kata Kalimat 1 : $jumlahKataKalimat1 <br> ";
        
        $kalimat2 = "I'm ready for the challenges";
        $panjangKalimat2 = strlen($kalimat2);
        $jumlahKataKalimat2 = str_word_count($kalimat2);

        echo "Kalimat 2 : $kalimat2 <br>";
        echo "panjang Kalimat2 : $panjangKalimat2 <br> ";
        echo "jumlah Kata Kalimat 2 : $jumlahKataKalimat2 <br> ";

        echo "<h3> Soal No 2</h3>";
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
           
        */
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata kedua: " ;
        echo "<br> Kata Ketiga: " ;

        echo "<h3> Soal No 3 </h3>";
        /*

     $kalimat2 = "I love PHP";
     $kata1Kalimat2= substr($kalimat2, 0);
     $kata2Kalimat2= substr($kalimat2,  2);
     $kata3Kalimat2= substr($kalimat2, 8);

    echo "Kata 1 Kalimat 2: " . substr($Kalimat2, 0) . "<br>"; 
    echo "Kata 2 Kalimat 2: " . substr($Kalimat2, 0) . "<br>";
    echo "Kata 3 Kalimat 2: $kata3Kalimat2 <br>";


        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "String: \"$string3\" "; 
        // OUTPUT : "PHP is old but awesome"

     $kalimat3 = "PHP is old but awesome";

        $gantiStringKalimat3 = str_replace("sexy", "awesome", $kalimat3);
        echo "Kalimat 3 : $kalimat3 <br>";
        echo "ubah kalimat 3 : $kalimat3 <br>";
    ?>
</body>
</html>